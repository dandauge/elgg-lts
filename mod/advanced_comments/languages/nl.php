<?php
/**
 * This file was created by Translation Editor v5.2
 * On 2017-01-11 12:17
 */

return array (
  'advanced_comments:settings:defaults' => 'Standaard reactie instellingen',
  'advanced_comments:settings:defaults:auto_load:help' => 'laad automatisch de volgende serie reacties in als de gebruiker het einde van de pagina bereikt',
  'advanced_comments:settings:defaults:user_preference' => 'Mogen gebruikers de reactie instellingen aanpassen',
  'advanced_comments:settings:helper' => 'Formulier ondersteuning',
  'advanced_comments:comment:logged_out' => 'Reageren is alleen toegestaan voor aangemelde gebruikers',
  'advanced_comments:settings:show_login_form' => 'Toon aanmeldformulier voor afgemelde gebruikers onder de reacties',
  'advanced_comments' => 'Geadvanceerde reacties',
  'advanced_comments:header:order' => 'Reactie volgorde',
  'advanced_comments:header:order:asc' => 'Oudste eerst',
  'advanced_comments:header:order:desc' => 'Nieuwste eerst',
  'advanced_comments:header:limit' => 'Aantal',
  'advanced_comments:header:auto_load' => 'Automatisch laden',
);
