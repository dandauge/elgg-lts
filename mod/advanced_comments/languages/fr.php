﻿﻿﻿﻿﻿﻿<?php

return array (
  'advanced_comments:settings:helper' => 'Formulaire',
  'advanced_comments:header:order' => 'Ordre des commentaires',
  'advanced_comments:header:order:asc' => 'Les plus anciens en premier',
  'advanced_comments:header:order:desc' => 'Les plus récents en premier',
  'advanced_comments:header:limit' => 'Limite',
  'advanced_comments:header:auto_load' => 'Chargement automatique',
  'advanced_comments:comment:logged_out' => 'Commentaires uniquement pour les utilisateurs connectés',
  'advanced_comments:settings:defaults' => 'Paramètres des commentaires par défaut',
  'advanced_comments:settings:defaults:auto_load:help' => 'Charger automatiquement le lot de commentaires suivant lorsque l\'utilisateur atteint la fin de la page',
  'advanced_comments:settings:defaults:user_preference' => 'Les utilisateurs sont-ils autorisés à modifier les paramètres des commentaires',
  'advanced_comments:settings:show_login_form' => 'Afficher le formulaire de connexion pour les utilisateurs déconnectés',
);
