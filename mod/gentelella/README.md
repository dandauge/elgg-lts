Gentelella theme for elgg
=============

Design
------

This is a port to elgg of the Gentelella Admin theme. Gentelella is a free to use Bootstrap admin template. This template uses the default Bootstrap 3 styles along with a variety of powerful jQuery plugins and tools to create a powerful framework for creating admin panels or back-end dashboards.

Layout
------

Gentelella for elgg theme is responsive and adapts the layout to the viewing environment using media queries and fluid images.


Requirements
------------

Elgg 2.3.x
It will totally break in Elgg 3.x


License Information
------------

Gentelella for elgg theme is licensed under The MIT License (MIT). Which means that you can use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software. But you always need to state that Colorlib is the original author of this template.

The Gentelella Admin Project is developed and maintained by Colorlib and Aigars Silkalns