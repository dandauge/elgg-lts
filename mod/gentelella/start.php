<?php
/**
 * gentelella theme plugin
 *
 * @package gentelellaTheme
 */

elgg_register_event_handler('init','system','gentelella_theme_init');

function gentelella_theme_init() {

	elgg_register_event_handler('pagesetup', 'system', 'gentelella_theme_pagesetup', 1000);

	// theme specific CSS
	elgg_extend_view('elgg.css', 'gentelella_theme/css');

        $custom_min_css_dir =  "/mod/gentelella/vendors/build/css/custom.min.css";
        $bootstrap_min_css = "/mod/gentelella/views/default/gentelella_theme/bootstrap_modified.css";
        $font_awesome_css = "/mod/gentelella/vendors/font-awesome/css/font-awesome.min.css";
        $np_progress_css = "/mod/gentelella/vendors/nprogress/nprogress.css";
        $bootstrap_progress_bar_css = "/mod/gentelella/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css";
        $bootstrap_date_range_css = "/mod/gentelella/vendors/bootstrap-daterangepicker/daterangepicker.css";
        
        
        elgg_register_css('bootstrap_min_css', $bootstrap_min_css );
        elgg_register_css('font_awesome_css', $font_awesome_css );
        elgg_register_css('np_progress_css', $np_progress_css );
        elgg_register_css('bootstrap_progress_bar_css', $bootstrap_progress_bar_css );
        elgg_register_css('bootstrap_date_range_css', $bootstrap_date_range_css );       
        elgg_register_css('custom_min_css', $custom_min_css_dir );
        
        
        
        
        $jquery_js = "/mod/gentelella/vendors/jquery/dist/jquery.min.js";
        $bootstrap_js = "/mod/gentelella/vendors/bootstrap/dist/js/bootstrap.min.js";
        $icheck_js = "/mod/gentelella/vendors/iCheck/icheck.min.js";
        $fastclick_js = "/mod/gentelella/vendors/fastclick/lib/fastclick.js";
        $np_progress_js = "/mod/gentelella/vendors/nprogress/nprogress.js";
        $gauge_js = "/mod/gentelella/vendors/gauge.js/dist/gauge.min.js";
        $bootstrap_progress_bar_js = "/mod/gentelella/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js";
        $skycons_js = "/mod/gentelella/vendors/skycons/skycons.js";
        $date_js = "/mod/gentelella/vendors/DateJS/build/date.js";
        $bootstrapdaterangepicker_js = "/mod/gentelella/vendors/bootstrap-daterangepicker/daterangepicker.js";
        $moment_min_js = "/mod/gentelella/vendors/moment/min/moment.min.js";
        $build_js = "/mod/gentelella/vendors/build/js/custom.min.js";
        
        elgg_register_js('jquery_js', $jquery_js);
        elgg_register_js('bootstrap_js', $bootstrap_js);
        elgg_register_js('icheck_js', $icheck_js);
        elgg_register_js('fastclick_js', $fastclick_js);
        elgg_register_js('gauge_js', $gauge_js);
        elgg_register_js('np_progress_js', $np_progress_js);
        elgg_register_js('bootstrap_progress_bar_js', $bootstrap_progress_bar_js);
        elgg_register_js('skycons_js', $skycons_js);
        elgg_register_js('date_js', $date_js);
        elgg_register_js('bootstrapdaterangepicker_js', $bootstrapdaterangepicker_js);
        elgg_register_js('moment_min_js', $moment_min_js);
        elgg_register_js('build_js', $build_js);
        
        //elgg_unregister_js('require_js');
        
        
        elgg_unextend_view('page/elements/header', 'search/header');
	elgg_extend_view('page/elements/sidebar', 'search/header', 0);
	 
        
        elgg_load_css('bootstrap_min_css');
        elgg_load_css('font_awesome_css');
      //  elgg_load_css('np_progress_css');
      //  elgg_load_css('bootstrap_progress_bar_css');
      //  elgg_load_css('bootstrap_date_range_css');
        elgg_load_css('custom_min_css');
        
     //   elgg_load_js('jquery_js');
        elgg_load_js('bootstrap_js');
       // elgg_load_js('fastclick_js');
     //   elgg_load_js('icheck_js');
     //   elgg_load_js('gauge_js');
    //    elgg_load_js('np_progress_js');
        elgg_load_js('bootstrap_progress_bar_js');
     //   elgg_load_js('skycons_js');
    //    elgg_load_js('date_js');
    //    elgg_load_js('bootstrapdaterangepicker_js');
       // elgg_load_js('moment_min_js');
        elgg_load_js('build_js');
        
        
	elgg_register_plugin_hook_handler('head', 'page', 'gentelella_theme_setup_head');

	// non-members do not get visible links to RSS feeds
	if (!elgg_is_logged_in()) {
		elgg_unregister_plugin_hook_handler('output:before', 'layout', 'elgg_views_add_rss_link');
	}

}

/**
 * Rearrange menu items
 */
function gentelella_theme_pagesetup() {

	if (elgg_is_logged_in()) {

		elgg_unregister_menu_item('topbar', array(
			'name' => 'account',
			'text' => elgg_echo('account'),
			'href' => "#",
			'priority' => 100,
			'section' => 'alt',
			'link_class' => 'elgg-topbar-dropdown',
		));
                elgg_unregister_plugin_hook_handler('register', 'menu:topbar', 'messages_register_topbar');
                
                $item = elgg_register_menu_item('topbar', 
                        array(
                            
                        'name' =>'settings',
                        'text' => elgg_echo('settings'),
                        'href' => "settings/user/" . elgg_get_logged_in_user_entity()->username,
                        
                        'class' => '',
                            ));

                
                $item = elgg_register_menu_item('topbar', 
                        array(
                            
                        'name' =>'friends',
                        'text' => elgg_echo('friends'),
                        'href' => "friends/" . elgg_get_logged_in_user_entity()->username,
                        
                        'class' => '',
                            ));
                
                if (elgg_is_active_plugin('site_notifications')) {
                $item = elgg_register_menu_item('topbar', 
                        array(
                            
                        'name' =>'sitenotifications',
                        'text' => elgg_echo('site_notifications:topbar'),
                        'href' => "site_notifications/view/" . elgg_get_logged_in_user_entity()->username,
                        
                        'class' => '',
                            ));
                }
                $item = elgg_register_menu_item('topbar', 
                        array(
                            
                        'name' =>'profile',
                        'text' => elgg_echo('profile'),
                        'href' => "profile/" . elgg_get_logged_in_user_entity()->username,
                        
                        'class' => '',
                            ));
                //site_notifications
		if (elgg_is_active_plugin('dashboard')) {
			$item = elgg_unregister_menu_item('topbar', 'dashboard');
			if ($item) {
				$item->setText(elgg_echo('dashboard'));
				$item->setSection('default');
				elgg_register_menu_item('site', $item);
			}
		}
		
		$item = elgg_get_menu_item('topbar', 'usersettings');
		if ($item) {
			$item->setParentName('account');
			$item->setText(elgg_echo('settings'));
			$item->setPriority(103);
		}

		$item = elgg_get_menu_item('topbar', 'logout');
		if ($item) {
			$item->setParentName('account');
			$item->setText(elgg_echo('logout'));
			$item->setPriority(104);
		}

		$item = elgg_get_menu_item('topbar', 'administration');
		if ($item) {
			$item->setParentName('account');
			$item->setText(elgg_echo('admin'));
			$item->setPriority(101);
		}

		if (elgg_is_active_plugin('site_notifications')) {
			$item = elgg_get_menu_item('topbar', 'site_notifications');
			if ($item) {
				$item->setParentName('account');
				$item->setText(elgg_echo('site_notifications:topbar'));
				$item->setPriority(102);
			}
		}

		if (elgg_is_active_plugin('reportedcontent')) {
			$item = elgg_unregister_menu_item('footer', 'report_this');
			if ($item) {
				$item->setText(elgg_view_icon('report-this'));
				$item->setPriority(500);
				$item->setSection('default');
				elgg_register_menu_item('extras', $item);
			}
		}
	}
}

/**
 * Register items for the html head
 *
 * @param string $hook Hook name ('head')
 * @param string $type Hook type ('page')
 * @param array  $data Array of items for head
 * @return array
 */
function gentelella_theme_setup_head($hook, $type, $data) {
	$data['metas']['viewport'] = array(
		'name' => 'viewport',
		'content' => 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0',
	);

	$data['links']['apple-touch-icon'] = array(
		'rel' => 'apple-touch-icon',
		'href' => elgg_get_simplecache_url('gentelella_theme/homescreen.png'),
	);

	return $data;
}
