<?php
/**
 * Site navigation menu
 *
 * @uses $vars['menu']['default']
 * @uses $vars['menu']['more']
 */

$default_items = elgg_extract('default', $vars['menu'], array());
$more_items = elgg_extract('more', $vars['menu'], array());

echo '<ul class="nav side-menu">';
foreach ($default_items as $menu_item) {
    
	echo elgg_view('navigation/menu/elements/item', array('item' => $menu_item));
}

if ($more_items) {
  
	echo "<li>";

	$more = elgg_echo('more');
	echo "<a>$more</a>";
	
	echo elgg_view('navigation/menu/elements/section', array(
		'class' => 'nav child_menu', 
		'items' => $more_items,
	));
	
	echo '</li>';
     
}
echo '</ul>';
