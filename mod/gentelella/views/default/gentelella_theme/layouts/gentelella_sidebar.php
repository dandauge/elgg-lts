<?php

/* 
 * Sidebar menu
 */
$navbar = elgg_view('page/elements/navbar', $vars);
$site_url = elgg_get_site_url();
$site = elgg_get_site_entity();
?>
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
               
              <div class="menu_section">
                <h3>General</h3>
               </br>
                    <?php 
                   echo $navbar;
                    
                     
                    ?>
                
              </div>
    <?php
    if (elgg_is_logged_in()) {
    
    ?>
              <div class="menu_section">
                <h3>Options</h3>
                <ul class="nav side-menu">
                    <?php 
                    if(elgg_is_admin_user(elgg_get_logged_in_user_entity()->guid))
                    {
                        
                    
                    ?>
                                     
                  <li><a href="<?php echo $site_url; ?>admin">
                          <i class="fa fa-cogs"></i>
                          <span class="label label-success "><?php echo elgg_echo('admin')?></span></a>
                  </li>
                  <?php
                    }
                   ?>       
                  <li><a href="<?php echo $site_url; ?>action/logout">
                          <i class="fa fa-laptop"></i>
                          <span class="label label-danger "><?php echo elgg_echo('logout')?></span></a>
                  </li>
                  
                  
                </ul>
              </div>
<?php
    }
    
    ?>
</div>