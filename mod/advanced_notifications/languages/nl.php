<?php
/**
 * This file was created by Translation Editor v6.0
 * On 2018-02-27 16:35
 */

return array (
  'advanced_notifications:settings:queue_delay' => 'Notificatie wachtrij vertraging (seconden)',
  'advanced_notifications:settings:queue_delay:help' => 'Items zullen uit de wachtrij worden opgepakt na de opgegeven vertraging',
  'advanced_notifications:settings:notify_owner_subscribers' => 'Verstuur een notificatie naar de abonnees van de eigenaar over nieuwe content',
  'advanced_notifications:settings:notify_owner_subscribers:help' => 'Standaard verstuurd Elgg een notificatie over nieuwe content naar de abonnees van de container (bijv. groep of gebruiker). Dit betekend dat als je notificaties wilt ontvangen over nieuwe content door een gebruiker en de content is in een groep geplaatst je geen notificatie ontvangt (tenzij je bent geabonneerd op de groep). Indien deze instelling aanstaat zullen de abonnees van de eigenaar ook een notificatie ontvangen.',
);
