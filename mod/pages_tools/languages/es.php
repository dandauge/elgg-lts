﻿<?php
/**
 * This file was created by Translation Editor v6.0
 * On 2017-10-07 16:00
 */

return array (
  'pages_tools:allow_comments' => 'Permitir comentarios',
  'pages_tools:unpublished' => 'Despublicar',
  'pages_tools:notify:edit:subject' => 'Su pagina \'%s\' fue editado',
  'pages_tools:notify:edit:message' => 'Hola,

Su pagina \'%s\' fue editado por %s. Echa un vistazo a la nueva versión aquí:
%s',
  'pages_tools:notify:publish:subject' => 'Se ha publicado una página',
  'pages_tools:notify:publish:message' => 'Hola,

su pagina \'%s\' ha sido publicada.

Puedes ver tu página aquí:
%s',
  'pages_tools:notify:expire:subject' => 'Una página ha caducado',
  'pages_tools:notify:expire:message' => 'Hola,

tu pagina \'%s\' ha expirado.

Puedes ver tu página aquí:
%s',
  'page_tools:export:format' => 'Formato de página',
  'page_tools:export:include_subpages' => 'Incluir subpáginas',
  'page_tools:export:include_index' => 'Incluir índice',
  'pages_tools:navigation:tooltip' => '¿Sabías que puedes arrastrar y soltar páginas para reordenar el árbol de navegación?',
  'pages_tools:widgets:index_pages:description' => 'Mostrar las últimas páginas de tu comunidad',
  'pages_tools:settings:advanced_publication' => 'Permitir opciones avanzadas de publicación',
  'pages_tools:settings:advanced_publication:description' => 'Con esto los usuarios pueden seleccionar una publicación y una fecha de caducidad para las páginas. Requiere un diario de trabajo CRON.',
  'pages_tools:label:publication_options' => 'Opciones de publicación',
  'pages_tools:label:publication_date' => 'Fecha de publicación (opcional)',
  'pages_tools:publication_date:description' => 'Cuando selecciona una fecha aquí, la página no se publicará hasta la fecha seleccionada.',
  'pages_tools:label:expiration_date' => 'Fecha de vencimiento (opcional)',
  'pages_tools:expiration_date:description' => 'La página ya no se publicará después de la fecha seleccionada.',
  'pages_tools:edit:confirm' => '¡Alguien está editando esta página!
¿Seguro que también desea editar esta página?',
  'pages_tools:export:index' => 'Contenido',
  'pages_tools:actions:reorder:error:subpages' => 'No se proporcionaron páginas para reordenar',
  'pages_tools:actions:reorder:success' => 'Re-ordenado con éxito las páginas',
  'page_tools:export:format:a4' => 'A4',
  'page_tools:export:format:letter' => 'Letter',
  'page_tools:export:format:a3' => 'A3',
  'page_tools:export:format:a5' => 'A5',
);
