<?php

return [
	'default' => [
		'ckeditor.js' => __DIR__ . '/vendors/ckeditor/4.6.2/ckeditor.js',
		'ckeditor/' => __DIR__ . '/vendors/ckeditor/4.6.2/',
		'jquery.ckeditor.js' => __DIR__ . '/vendors/ckeditor/4.6.2/adapters/jquery.js',
	],
];
