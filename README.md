Elgg Starter Project [![Build Status](https://travis-ci.org/Elgg/starter-project.svg?branch=master)](https://travis-ci.org/Elgg/starter-project)
====
En
This is a quickstart skeleton for building a site based on Elgg 2.x.

Fr
Voici une pile d'installation d'Elgg LTS 2.x.

## Installation

En
Follow the instructions for [installing Elgg with Composer](http://learn.elgg.org/en/stable/intro/install.html#overview). Otherwise, download the zip and unzip it to the folder of your choice.

Fr
Pour installer Elgg avec Composer, suivre les instructions [de la documentation officielle](http://learn.elgg.org/en/stable/intro/install.html#overview). Autrement, téléchargez le zip et décompressez-le à l'endroit de votre choix.

## Composants additionnels

En
In the /Mod directory a number of modules have been added in complement to those originally included. They have been selected because they are all followed by people who have been using the tool for a long time.

FR
Dans le répertoire /Mod ont été rajoutés un certain nombre de modules en plus de ceux fournis initialement. Ils ont été sélectionnés car ils sont tous suivis par des personnes présentent depuis longtemps sur l’outil (sauf le thème).

Liste des modules rajoutés :

- Advanced Comments
- Blog tools
- CKEditor Extended
- Cookie banner
- Discussions Tools
- Elgg Update Services
- Embed Extended
- Event Manager
- FreiChat
- Group Tools
- Image Enlarger
- iZAP Videos - revised edition by iionly
- Advanced Notifications
- Pages Tools
- Poll
- Profile Manager
- Tag Tools
- Tidypics
- Image Enlarger
- Target Blank
- The Wire Website Cards
- Translation Editor
- Widget Manager
- Gentelella Theme
